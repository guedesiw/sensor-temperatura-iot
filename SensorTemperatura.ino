#include <DHT.h>
#include <ESP8266WiFi.h>
#define DHTPIN 5
DHT dht(DHTPIN, DHT11);

const int ledPinYellow = 13;
const int ledPinBlank = 12;

const char *ssid = "Guedes";
const char *pass = "senha";

const char* server = "api.thingspeak.com";

String apiKey = "api-key";

String thingtweetAPIKey = "api-twiter-key";

WiFiClient client;

void setup() {
  pinMode(ledPinYellow, OUTPUT);
  pinMode(ledPinBlank, OUTPUT);

  Serial.begin(115200);
  delay(10);
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
  }
  dht.begin();
}

void loop() {
  delay(5000);
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  
  showInMonitor(t,h);
  sendToThingView(t,h);
  
  if(WiFi.status() == WL_CONNECTED){
    digitalWrite(ledPinYellow, HIGH); // Acende o Led amarelo
  }else{
    digitalWrite(ledPinYellow, LOW); // Acende o Led amarelo
  }
  
  if(t > 30.0f) {
    digitalWrite(ledPinBlank, HIGH); // Acende o Led branco
    updateTwitterStatus("Tweet enviado pela NodeMCU sobre a temperatura acima de 25ºC no Lab de Robótica");
  }else{
    digitalWrite(ledPinBlank, LOW); // Acende o Led branco
  }
}

void showInMonitor(float t, float h){
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.print(" degrees Celcius, Humidity: ");
  Serial.print(h);
  Serial.println("%. Send to Thingspeak.");
  if(t > 30.0f) {
    Serial.println("Led branco aceso, temperatura maior que 30 t="+String(t));
  }else{
    Serial.println("Led Branco Apagado, temperatura menor que 30 t="+String(t));
  }
  if(WiFi.status() == WL_CONNECTED){
    Serial.println("Led amarelo aceso, conectado no wifi com sucesso.");
  }else{
    Serial.println("Led amarelo apagado, desconectado do wifi.");
  }
}

void sendToThingView(float t, float h){
  if (client.connect(server, 80))  //   "184.106.153.149" or api.thingspeak.com
  {
    String postStr = apiKey;
    postStr += "&field1=";
    postStr += String(t);
    postStr += "&field2=";
    postStr += String(h);
    postStr += "\r\n\r\n";
    client.print("POST /update HTTP/1.1\n");
    client.print("Host: api.thingspeak.com\n");
    client.print("Connection: close\n");
    client.print("X-THINGSPEAKAPIKEY: " + apiKey + "\n");
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("Content-Length: ");
    client.print(postStr.length());
    client.print("\n\n");
    client.print(postStr);
    Serial.print("Temperature: ");
    Serial.print(t);
    Serial.print(" degrees Celcius, Humidity: ");
    Serial.print(h);
    Serial.println("%. Send to Thingspeak.");
  }
  client.stop();
  Serial.println("Waiting...");
}

void updateTwitterStatus(String tsData)
{
  if (client.connect(server, 80))
  {
//  Create HTTP POST Data
    tsData = "api_key=" + thingtweetAPIKey + "&status=" + tsData;

    client.print("POST /apps/thingtweet/1/statuses/update HTTP/1.1\n");
    client.print("Host: api.thingspeak.com\n");
    client.print("Connection: close\n");
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("Content-Length: ");
    client.print(tsData.length());
    client.print("\n\n");

    client.print(tsData);
  }
}
